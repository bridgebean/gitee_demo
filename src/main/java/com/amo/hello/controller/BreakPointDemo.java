package com.amo.hello.controller;

import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;

/**
 * @ClassName BreakPointDemo
 * @Description TODO
 * @Author Bridge
 * @Date 2021/6/6 16:06
 * @Version 1.0
 **/

public class BreakPointDemo {

    public static void line(){
        System.out.println("this is line breakPoint");
    }


    public static void detailLint(){
        System.out.println("this is detailLine breakpint");
    }


   public static void mthod() {
       System.out.println("this is method");
       Service service = new ServiceImpl();
       service.excute();
   }


   public static void exception(){
        Object o = null;
        o.toString();
       System.out.println("this is never be print");

   }


   public static void firld() {
       Person person = new Person("amo",18);
       person.setAge(20);

       System.out.println(person);
   }

    public static void main(String[] args) {
        //line();
       // detailLint();
       // mthod();
        //exception();
        firld();

    }
}

    
    