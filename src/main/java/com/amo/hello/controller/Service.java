package com.amo.hello.controller;

/**
 * @InterfaceName Service
 * @Description TODO
 * @Author Bridge
 * @Date 2021/6/6 16:38
 * @Version 1.0
 **/

public interface Service {
    String excute();
}
