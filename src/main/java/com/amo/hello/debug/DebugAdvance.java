package com.amo.hello.debug;

import com.amo.hello.controller.Person;
import com.amo.hello.controller.Service;
import com.amo.hello.controller.ServiceImpl;
import sun.java2d.pipe.SpanIterator;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName DebugAdvance
 * @Description TODO
 * @Author Bridge
 * @Date 2021/6/6 16:58
 * @Version 1.0
 **/

public class DebugAdvance {

    // 条件表达式
    public static void conditions(){
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        MyThread myThread = new MyThread();
        Thread t1 = new Thread(myThread,"thread1");
        Thread t2 = new Thread(myThread,"thread2");
        Thread t3 = new Thread(myThread,"thread3");
        t1.start();
        t2.start();
        t3.start();
    }

    // 打印堆栈信息
    public static void printstackTrace(){
        ArrayList list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        System.out.println(list);
    }

    // 表达式解析
    public static void evaluate(){
        System.out.println("evalue");
        Person p = new Person("bywind", 23);
        List<Integer> list= Arrays.asList(1,2,3,4).stream().map(x->x * 2).collect(Collectors.toList());
    }

    // 避免操作资源
    public static void saveResource(){
        System.out.println("shit happens");
        System.out.println("save to db");
        System.out.println("save to redis");
        System.out.println("send message to mq");
    }

    public static void keys(){
        System.out.println("keys");

        System.out.println("steo over");

        System.out.println("strp into | step out");

        Service service = new ServiceImpl();

        service.excute();

        StringBuffer sb = new StringBuffer();
        sb.append("hello world");
        System.out.println(sb.toString());


        System.out.println("I am here");
        System.out.println("I am here");
        System.out.println("I am here");
        System.out.println("I am here");
        System.out.println("I am here");
    }

    public static void sourceCode(){
        ArrayList arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        System.out.println(arrayList.size());


        LinkedList linkedList = new LinkedList();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        System.out.println(linkedList.size());
    }

    public static void main(String[] args) {
        //conditions();
        //printstackTrace();
        //evaluate();
        //saveResource();
        // keys();
        //sourceCode();

        stramDebug();

        System.out.println("hhhh");

        Object o = null;
        if (o != null) {

            for (int i = 0; i < 200; i++) {

            }
        }

    }

    private static void stramDebug() {

        Arrays.asList(1,2,3,45).stream().filter(i->i  % 2 == 0 || i%3==0).map(i-> i * i).forEach(System.out::println);

    }
}

    
    