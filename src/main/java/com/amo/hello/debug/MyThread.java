package com.amo.hello.debug;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName MyThread
 * @Description TODO
 * @Author Bridge
 * @Date 2021/6/6 16:57
 * @Version 1.0
 **/

public class MyThread implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"进入");

        try {
            TimeUnit.MICROSECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(Thread.currentThread().getName()+"离开");
        }
    }
}

    
    